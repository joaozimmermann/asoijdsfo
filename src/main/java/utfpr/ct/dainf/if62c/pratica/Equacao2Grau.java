/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Joao
 * @param <Number>
 */
public class Equacao2Grau<T extends Number>{
    T a;
    T b;
    T c;

    public Equacao2Grau() {
    }

    public Equacao2Grau(T a, T b, T c) throws RuntimeException{
        if (a.equals(0))
            throw new RuntimeException("Coeficiente a não pode ser zero");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public T getA() {
        return a;
    }

    public T getB() {
        return b;
    }

    public T getC() {
        return c;
    }

    public void setA(T a) {
        if (a.equals(0))
            throw new RuntimeException("Coeficiente a não pode ser zero");
        this.a = a;
    }

    public void setB(T b) {
        this.b = b;
    }

    public void setC(T c) {
        this.c = c;
    }
    
    public double getRaiz1(){
        double delta=0;
        double aa, bb, cc;
        aa = a.doubleValue();
        bb = b.doubleValue();
        cc = c.doubleValue();
        delta = Math.pow(bb, 2) +(-4*aa*cc);
        if (delta<0)
            throw new RuntimeException("Equação não tem solução real");
        else{
            return (-bb-Math.sqrt(+delta))/(2*aa);
        }
    }
    public double getRaiz2(){
        double delta=0;
        double aa, bb, cc;
        aa = a.doubleValue();
        bb = b.doubleValue();
        cc = c.doubleValue();
        delta = -(4*aa*cc);
        delta += Math.pow(bb, 2);
        if (delta<0)
            throw new RuntimeException("Equação não tem solução real");
        else
            return (-bb+Math.sqrt(+delta))/(2*aa);
    }
}
