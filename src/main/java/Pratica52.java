
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {
    public static void main(String[] args) throws RuntimeException {
        try{
            Equacao2Grau<Integer> a = new Equacao2Grau<>(1, 2, -3);
            System.out.println(a.getRaiz1());
            System.out.println(a.getRaiz2());
        }catch(RuntimeException aa){
            System.out.println(aa.getLocalizedMessage());
        }
        try{
            Equacao2Grau<Integer> b = new Equacao2Grau<>(1, 4, 4);
            System.out.println(b.getRaiz1());
            System.out.println(b.getRaiz2());
        }catch(RuntimeException aa){
            System.out.println(aa.getLocalizedMessage());
        }        
        try{
            Equacao2Grau<Integer> b = new Equacao2Grau<>(1, 4, 6);
            System.out.println(b.getRaiz1());
            System.out.println(b.getRaiz2());
        }catch(RuntimeException aa){
            System.out.println(aa.getLocalizedMessage());
        } 
    }
}
